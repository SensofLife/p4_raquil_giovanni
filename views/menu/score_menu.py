from .menu import Menu
from models.player import Player


class ScoreMenu(Menu):
    """Choix du score d'un match """
    def __init__(self, j1: Player, j2: Player):
        super().__init__(title="score", choices=[
                (f"{j1} a gagné", 1.0),
                (f"{j2} a gagné ", 0.0),
                (f"{j1} et {j2} sont a égalité", 0.5)
        ])
