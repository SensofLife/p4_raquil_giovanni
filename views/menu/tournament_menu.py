from .menu import Menu


class TournamentMenu(Menu):
    """Menu de l'écran tournoi """
    def __init__(self):
        super().__init__(title="menu tournoi", choices=[
            ("creer un tournoi", "/tournaments/create"), ("lancement d'un tournoi", "/tournaments/play"),
            ("liste des tournois ", "/tournaments/list"), ("retour au menu précédent", "/"),
        ])
