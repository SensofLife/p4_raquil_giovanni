from .menu import Menu


class MainMenu(Menu):
    """Menu Principal """
    def __init__(self):
        super().__init__(title="menu principale", choices=[
            ("menu joueurs", "/players"),
            ("menu tournoi", "/tournaments"),
            ("rapport", "/rapport"),
            ("quitter", "/exit")
            ])
