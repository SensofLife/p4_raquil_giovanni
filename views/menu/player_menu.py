from .menu import Menu


class PlayerMenu(Menu):
    """Menu de l'écran joueur """
    def __init__(self):
        super().__init__(title="menu Joueur", choices=[
                ("creer un nouveau joueur", "/players/create"),
                ("afficher les joueurs ", "/players/list"),
                ("Changer le classement d'un joueur", "/players/change_rank"),
                ("retour au menu précédent", "/")
        ])
