from .menu import Menu


class TimeMenu(Menu):
    """Menu du choix du type de temps pour le tournoi """
    def __init__(self):
        super().__init__(title="choix du temps de jeux", choices=[
                ("Bullet", "1"),
                ("Blitz", "2"),
                ("quick_hit", "3")
        ])
