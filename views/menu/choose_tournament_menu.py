from .menu import Menu


class ChooseTournamentMenu(Menu):
    """Menu de choix d'un tournoi """
    def __init__(self, tournament):
        super().__init__(title="choix du tournoi", choices=[
                (str(tournament), tournament.id) for tournament in tournament])
