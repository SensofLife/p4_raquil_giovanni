from .menu import Menu


class ChoosePlayerMenu(Menu):
    """Menu du choix du joueur """
    def __init__(self, player):
        super().__init__(title="choix du joueur", choices=[
                (str(player), player.id) for player in player])
