from .menu import Menu


class ContinuMenu(Menu):
    """Choix pour continuer un tournoi aprés un tour """
    def __init__(self):
        super().__init__(title="Voulez-vous poursuivre au tour suivant ? ", choices=[
                ("OUI", "O"),
                ("Non", "N")
        ])
