from .menu import Menu


class ListPlayerMenu(Menu):
    """Menu de sélection de joueur """
    def __init__(self):
        super().__init__(title="liste des joueurs trier par :", choices=[
                ("liste des joueurs trier par nom", "/player/list/name"),
                ("liste des joueurs trier par rang", "/players/list/rank"),
                ("retour au menu principal", "/")
        ])
