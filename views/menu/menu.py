from views.view import View
from typing import List, Tuple


class Menu(View):
    """class pour l'affichage des choix """
    def __init__(self, title: str, choices: List[Tuple[str, str]]):
        self.choices = choices
        self.title = title
        super().__init__(title, "\n".join([f"{i+1}-{c}" for i, (c, path) in enumerate(choices)]))

    def show(self):
        super().show()
        return self.choice()

    def choice(self) -> str:
        """Permet de choisir un choix parmis les possibilité donnée
                return A(str):
                    choix de l'utilisateur """
        while True:
            if len(self.choices) == 0:
                print(f"le {self.title} est impossible. \nil n'y a pas d'élément\n")
                break
            choice_str = input(f"Veuillez faire un choix entre 1 et {len(self.choices)} : ")
            print("")
            try:
                choice_int = int(choice_str)
                if not 1 <= choice_int <= len(self.choices):
                    continue
                return self.choices[choice_int-1][1]

            except ValueError:
                pass
