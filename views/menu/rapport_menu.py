from .menu import Menu


class RapportMenu(Menu):
    """Menu de l'écran rapport """
    def __init__(self):
        super().__init__(title="rapport", choices=[
                ("Liste des joueurs", "/rapport/player/list"),
                ("liste des tournois", "/rapport/tournament/list"),
                ("liste des joueurs d'un tournoi", "/rapport/tournament/list_players"),
                ("liste des tours d'un tournoi et ses matchs", "/rapport/tournament/list_round"),
                ("retour au menu précédent", "/")
        ])
