from .form import Form


class TournamentForm(Form):
    """ class de gestion des données pour la creation d 'un nouveau tournoi """
    def __init__(self):
        super().__init__(
            title="creation d'un tournoi", fields=[
                ("name", "nom du tournoi", str),
                ("place", "lieu du tournoi", str),
                ("start_date_day", "jours de début", int),
                ("start_date_month", "mois de début", int),
                ("start_date_year", "année de début", int),
                ("start_hour", "heure de début du tournoi ", int),
                ("start_minute", "minute de début du tournoi ", int),
                ("end_date_day", "jours de fin", int),
                ("end_date_month", "mois de fin", int),
                ("end_date_year", "année de fin", int),
                ("end_hour", "heure de fin du tournoi", int),
                ("end_minute", "minute de fin du tournoi", int),
                ("nb_of_round", "nombre de tour", int),
                ("nb_of_player", "nombre de joueurs", int),
                ("description", "description", str)
            ])
