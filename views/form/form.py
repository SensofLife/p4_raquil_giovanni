from views.view import View

from typing import List, Tuple, Any


class Form(View):
    """ class de gestion des donnée entré par l'utilisateur """
    def __init__(self, title, fields: List[Tuple[str, str, Any]]):
        super().__init__(title=title)
        self.fields = fields

    def show(self):
        """ permet l'affichage des donnée """
        data = {}
        super().show()
        for field_name, field_description, field_type in self.fields:
            data[field_name] = self.check(field_description, field_type)
        return data

    def check(self, field_description, field_type):
        """Permet de récupérer et de vérifier les information entrées par l'utilisateur
            param A(str):
                nom de la donnée a récupérer
            pram B(Type):
                type de la donnée a récupérer

            return A(Value):
                valeur dans le type demandé """
        while True:
            field_value = input(f"{field_description}? ")
            try:
                return field_type(field_value)
            except ValueError:
                pass
