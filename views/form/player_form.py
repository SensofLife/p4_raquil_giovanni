from .form import Form


class PlayerForm(Form):
    """ class de gestion des données pour la creation d'un nouveau joueur """
    def __init__(self):
        super().__init__(
            title="creation d'un joueur", fields=[
                ("last_name", "Nom", str),
                ("first_name", "Prénom", str),
                ("birth_date_day", "Jour de naissance", int),
                ("birth_date_month", "Mois de naissance", int),
                ("birth_date_year", "Année de naissance", int),
                ("gender", "Sexe", str),
                ("rank", "Classement", int),
            ])
