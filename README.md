# ChessMaster :
    Ce programme permet de planifier l'organisation d'un tournoi d 'échec et ses joueurs, et de lancer se tournoi 

---

 ## Pré-requis :
   Nous utiliserons python3 pour ce programme ainsi que la librairie:
   - tyniDB
   - flake8-html

---

 ## installation :
 1. Dans un terminal entrez les commandes:
   - ``git clone https://graquil@bitbucket.org/SensofLife/p4_raquil_giovanni.git``
   - ``cd p4_raquil_giovanni``

1. Mise en place de l'environnement virtuel:
   1. sous Windows:
      - ``python -m venv env``
      - ``source env/scripts/activate``
   
   2. sous Unix:
      - ``python3 -m venv env``
      - ``source env/bin/activate``

   

2. Mise en place des librairie nécéssaire
   1. ```pip install -r requirement.txt``` 
---

## Creation du fichier flake8-html
- **Dans une invite de commande aprés avoir activer l'environnement virtuel**
  - ``flake8 --format=html --htmldir=flake-report``

## Lancement du programme:

- **Pour lancer le programme**

1. sous windows
    - ``` python main.py```
2. sous UNIX
   - ```python3 main.py```

## Navigation a l'intérieur du programme

- **Pour naviger dans le programme il faut appuyer sur la touche correspondante a l'action souhaitée**


