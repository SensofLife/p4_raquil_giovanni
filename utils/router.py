class Router():
    """class de direction entre les différent écran de l'interface"""
    def __init__(self):
        self.routes = []

    def navigate(self, path):
        """ Navigation vers les écran
            param A(str):
                chemin de l'écran
        """
        for p, controller in self.routes:
            if p == path:
                controller()

    def add_route(self, path, controller):
        """Permet de rajouter un route
            param A(str):
                chemin de la nouvelle route
            param B(function):
                fonction lier a la route """
        self.routes.append((path, controller))


router = Router()
