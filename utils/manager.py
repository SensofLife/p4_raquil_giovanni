from tinydb import TinyDB, Query
from typing import Any, Callable
import os

root_directory = os.path.dirname(os.path.realpath(__file__))
parent_directory = os.path.dirname(root_directory)


class Manager:
    """classe de gestion des joueurs ou des tournois
    """
    def __init__(self, item_type: Any, id_finder: Callable[[Any], Any]) -> None:
        self.item_type = item_type
        self.items = {}
        self.id_finder = id_finder
        self.file_name = f"{self.item_type.__name__.lower()}s.json"
        path = self.create_folder()
        self.db = TinyDB(path, sort_keys=True, indent=4)

    def create_folder(self):
        path = os.path.join(parent_directory, "json")
        if not os.path.exists(path):
            os.makedirs(path)
        return os.path.join(path, self.file_name)

    def save(self):
        """permet de sauvegarder dans la base de donnée"""
        self.db.truncate()
        items = self.find_all()
        for item in items:
            self.db.insert(item.serialize())

    def load_from_json(self):
        """permet de charger les infos qui sont dans la base de donnée"""
        for item_data in self.db.all():
            self.create_item(**item_data)

    def create_item(self, **kwargs: dict):
        """creait un objets de la classe donnée grace a un dictionnaire
            para A(dict):
                dictionnaire a parser

            return A(bool)
                booléen de réussite ou échec de la création de la classe
            return B(exception)
                -érreur relever
        """
        try:
            item = self.item_type(**kwargs)
            self.items[self.id_finder(item)] = item
            return True, None
        except Exception as e:
            return False, e

    def find_all(self):
        """
        permet d'avoir tout les éléments de la base de donnée

            return (list):
                liste des éléments de la base de donnée
        """
        return list(self.items.values())

    def find_by_id(self, item_id: str):
        """permet de trouver un élément spécifique
        grace a son ID dans la base de donnée
            param A(str):
                id de l'objet recherché

            return(item):
                objets correspondant a l'id donnée
        """
        return self.items[item_id]

    def update(self, item_id: str, fields, value):
        """permet de mettre a jour un élément de la base de donnée grace a son ID
        """
        User = Query()
        self.db.update({fields: value}, User.id == item_id)
