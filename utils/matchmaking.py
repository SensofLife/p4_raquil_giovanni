from typing import List
from datetime import datetime

from models.match import Match
from models.round import Round
from models.tournament import Tournament


def name_machups(p1, p2):
    """permet de creer une instance de la class Match
        param A(class Player):
            joueur 1 du match
        param B(class Player):
            joueur 2 du match

        return A(class Math):
            instance class match"""
    j1 = min(str(p1.id), str(p2.id))
    j2 = max(str(p1.id), str(p2.id))
    return Match(j1, j2, 0.0, 0.0)


def sorted_player_by_score(tournament, liste_match: List[Match]):
    """Permet de trier les joueurs par score et classement
        return A(list):
            liste des joueurs d'un tournoi trier par score puis par classement """
    return sorted(tournament.players, key=lambda z: (-get_player_score(z.id, liste_match), -z.rank))


def generate_round(tournament: Tournament) -> Round:
    """ generateur de round
        param A(class Tournament):
            tournoi en cours

        return A(class Round):
            instance de la class Round """
    liste_match = []
    for rnd in tournament.rounds:
        for match in rnd.matches:
            liste_match.append(match)
    if len(tournament.rounds) == 0:
        list_player_sorted = sorted(tournament.players, key=lambda x: -x.rank)
        half_nb_players = len(list_player_sorted)//2
        liste1 = list_player_sorted[:half_nb_players]
        liste2 = list_player_sorted[half_nb_players:]
        liste_match_round = []
        for i in range(half_nb_players):
            liste_match_round.append(name_machups(liste1[i], liste2[i]))

    else:
        liste_match_round = []
        list_player_sorted = sorted_player_by_score(tournament, liste_match)
        i = 1
        while len(list_player_sorted) != 0:
            list_name_match = [str(match) for match in liste_match]
            j1 = list_player_sorted[0]
            try:
                j2 = list_player_sorted[i]
            except IndexError:
                list_name_match = []
                pass
            match = name_machups(j1, j2)
            if str(match) not in list_name_match:

                list_player_sorted.remove(j1)
                list_player_sorted.remove(j2)
                i = 1

                liste_match_round.append(match)

            else:
                i += 1

    r = Round(name=f"tour{len(tournament.rounds)+1}",
              start_date=datetime.now(),
              matches=liste_match_round)
    return r


def get_player_score(p_id, scored_matches: List[Match]) -> float:
    """ Lecture du score d'un joueur
        param A(UUID):
            ID du joueurs
        param B(list):
            liste des matchs qui ont eux lieu

        return A(int):
            valeur de l'addition du score d'un joueur"""
    s = 0
    for match in scored_matches:
        if str(p_id) == match.id_j1:
            s += match.score1
        elif str(p_id) == match.id_j2:
            s += match.score2
    return s
