from models.tournament import tournament_manager
from models.player import player_manager
from utils.router import router
from controllers import (
    main,
    players,
    create_player,
    player_list,
    players_list_by_name,
    players_list_by_rank,
    change_rank,
    tournaments,
    create_tournament,
    tournaments_list,
    tournaments_play,
    rapport,
    rapport_player_list,
    rapport_tournament_list,
    rapport_tournament_player_list,
    rapport_tournament_list_round
)
tournament_manager.load_from_json()
player_manager.load_from_json()

# menu principale
router.add_route("/", main)

# menu joueurs
router.add_route("/players", players)
router.add_route("/players/create", create_player)
router.add_route("/players/list", player_list)
router.add_route("/player/list/name", players_list_by_name)
router.add_route("/players/list/rank", players_list_by_rank)
router.add_route("/players/change_rank", change_rank)

# menu tournament
router.add_route("/tournaments", tournaments)
router.add_route("/tournaments/create", create_tournament)
router.add_route("/tournaments/list", tournaments_list)
router.add_route("/tournaments/play", tournaments_play)

# menu rapport
router.add_route("/rapport", rapport)
router.add_route("/rapport/player/list", rapport_player_list)
router.add_route("/rapport/tournament/list", rapport_tournament_list)
router.add_route("/rapport/tournament/list_players", rapport_tournament_player_list)
router.add_route("/rapport/tournament/list_round", rapport_tournament_list_round)

# retour en arrière
router.navigate("/")
