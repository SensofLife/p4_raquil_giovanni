
from typing import List, Union, Dict
from datetime import datetime
from enum import Enum
from uuid import UUID
import re

from .player import Player, player_manager
from utils.manager import Manager
from .round import Round
from .match import Match


class Tournament():
    """classe des tournois
    """
    class Time(Enum):
        Bullet = "1"
        Blitz = "2"
        quick_hit = "3"

        def __str__(self):
            if self is Tournament.Time.Bullet:
                return "1"
            elif self is Tournament.Time.Blitz:
                return "2"
            else:
                return "3"

    def __init__(
        self, name: str, place: str, players: Union[List[Player], List[UUID]],
        time: Union[str, Time], rounds: Union[List[Round], List[Dict]],
        description: str = "",
        nb_of_round: int = 4, current_round: int = 0,
        start_date: Union[str, datetime] = datetime.today(),
        end_date: Union[str, datetime] = datetime.today(),
        id: str = None
    ):
        self.name = name
        self.place = place
        self.players = players
        self.time = time
        self.rounds = rounds
        self.description = description
        self.nb_of_round = nb_of_round
        self.current_round = current_round
        self.start_date = start_date
        self.end_date = end_date
        self.id = id

    def __str__(self):
        return f"{self.name}-{self.place}-{self.start_date.isoformat()}"

    @property
    def name(self) -> str:
        return self.__name

    @property
    def place(self) -> str:
        return self.__place

    @property
    def start_date(self) -> datetime:
        return self.__start_date

    @property
    def end_date(self) -> datetime:
        return self.__end_date

    @property
    def nb_of_round(self) -> int:
        return self.__nb_of_round

    @property
    def current_round(self) -> int:
        return self.__current_round

    @property
    def players(self) -> Union[List[Player], List[UUID]]:
        return self.__players

    @property
    def time(self) -> Union[str, Time]:
        return self.__time

    @property
    def description(self) -> str:
        return self.__description

    @property
    def id(self) -> str:
        return f"{self.name}-{self.place}-{self.start_date.isoformat()}"

    @property
    def rounds(self) -> Union[List[Round], List[Dict]]:
        return self.__rounds

    @property
    def matches(self) -> List[Match]:
        list_tournament = []
        for r in self.rounds:
            list_tournament += r.matches
        return list_tournament

    @property
    def is_over(self) -> bool:
        return len(self.rounds) == self.nb_of_round

    @id.setter
    def id(self, value: str):
        self.__id = value

    @name.setter
    def name(self, value: str):
        if not isinstance(value, str):
            raise AttributeError("Veuillez rentrez une chaine de caractère ")
        else:
            e = re.match("^([a-zA-Z](\\s?|-?)){2,25}$", value)
            if e:
                self.__name = value.upper()
            else:
                raise AttributeError("Le nom entré n'est pas au bon format")

    @place.setter
    def place(self, value: str):
        if not isinstance(value, str):
            raise AttributeError("Veuillez rentrez une chaine de caractère ")

        if len(value) > 25:
            raise AttributeError("le lieux dois avoir maximum 25 charactères")
        else:
            self.__place = value.upper()

    @start_date.setter
    def start_date(self, value: Union[str, datetime]):
        if isinstance(value, str):
            try:
                value = datetime.fromisoformat(value)
            except ValueError:
                raise AttributeError("La date entré n'est pas au bon format (YYYY-MM-DD)")

        if isinstance(value, datetime):
            if value < datetime.today():
                raise AttributeError("""le tournoi ne peux pas avoir lieu avant la date d'aujourd'hui
                veuillez vérifier la date """)

            self.__start_date = value

    @end_date.setter
    def end_date(self, value: Union[str, datetime]):
        if isinstance(value, str):
            try:
                value = datetime.fromisoformat(value)
            except ValueError:
                raise AttributeError("La date entré n'est pas au bon format (YYYY-MM-DD)")

        if isinstance(value, datetime):
            if value < self.start_date:
                raise AttributeError("le tournoi ne peux pas avoir lieu avant son commencement ")

            self.__end_date = value

    @nb_of_round.setter
    def nb_of_round(self, value: int):
        if not isinstance(value, int):
            raise AttributeError("veuillez entrez un chiffre ")

        self.__nb_of_round = value

    @current_round.setter
    def current_round(self, value: int):
        if not isinstance(value, int):
            raise AttributeError("veuillez entrez un chiffre ")
        self.__current_round = value

    @players.setter
    def players(self, value=Union[List[Player], List[UUID]]):
        player_manager.load_from_json()
        list_of_player = [
            player if isinstance(player, Player) else player_manager.find_by_id(player) for player in value
        ]
        self.__players = list_of_player

    @time.setter
    def time(self, value: Union[str, Time]):

        if isinstance(value, str):
            try:
                value = Tournament.Time(value)
            except ValueError:
                raise AttributeError("""Veuillez rentrer un numéro soit:
        -1 pour Bullet
        -2 pour Blitz
        -3 pour Coup rapide""")

        if isinstance(value, Tournament.Time):
            self.__time = value
        else:
            raise AttributeError("Le type de time entré n'est pas reconnue")

    @description.setter
    def description(self, value: str):
        if not isinstance(value, str):
            raise AttributeError("veuillez rentrez une chaine de caractère")
        if len(value) > 250:
            raise AttributeError("la description dois contenir maximum 250 charactères")

        self.__description = value

    @rounds.setter
    def rounds(self, value: Union[List[Round], List[Dict]]):
        self.__rounds = []
        for x in value:
            if isinstance(x, dict):
                x = Round(**x)
            else:
                raise AttributeError(f"le type de donner{type(x)} entré n'est pas correct")
            self.__rounds.append(x)

    def serialize(self):
        """
            fonction pour sérialiser la class tournament
            return: dict
        """
        list_id_players = [str(player.id) for player in self.players]
        list_rounds = []
        list_matches = []
        # serialisation classe Round
        for rnd in self.rounds:
            # serialisation classe Match
            for match in rnd.matches:
                info_match = match.serialize
                list_matches.append(info_match)

            info_rounds = rnd.serialize()
            list_rounds.append(info_rounds)

        return {
            "id": self.id,
            "name": self.name,
            "place": self.place,
            "start_date": str(self.start_date),
            "end_date": str(self.end_date),
            "nb_of_round": self.nb_of_round,
            "current_round": self.current_round,
            "players": list_id_players,
            "time": self.time.value,
            "rounds": list_rounds,
            "description": self.description
        }


tournament_manager = Manager(Tournament, lambda x: x.id)
