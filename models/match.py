class Match:
    """classe des matchs
    """
    def __init__(
        self,
        id_j1: str,
        id_j2: str,
        score1: float,
        score2: float
    ):
        self.id_j1 = id_j1
        self.id_j2 = id_j2
        self.score1 = score1
        self.score2 = score2

    def __str__(self):
        return f"{self.id_j1} VS {self.id_j2}"

    @property
    def id_j1(self) -> str:
        return self.__id_j1

    @property
    def id_j2(self) -> str:
        return self.__id_j2

    @property
    def score1(self) -> float:
        return self.__score1

    @property
    def score2(self) -> float:
        return self.__score2

    @id_j1.setter
    def id_j1(self, value: str):
        if not isinstance(value, str):
            raise ValueError(" le type d'identifiant entré n'est pas correct")
        self.__id_j1 = value

    @id_j2.setter
    def id_j2(self, value: str):
        if not isinstance(value, str):
            raise ValueError(" le type d'identifiant entré n'est pas correct")
        self.__id_j2 = value

    @score1.setter
    def score1(self, value: float):
        if not isinstance(value, float):
            raise ValueError("le score entré n'est pas correct")
        self.__score1 = value

    @score2.setter
    def score2(self, value: float):
        if not isinstance(value, float):
            raise ValueError("le score entré n'est pas correct")
        self.__score2 = value

    def serialize(self):
        """
            fonction pour sérialiser la class match
            return: dict
        """
        return {
            "id_j1": self.id_j1,
            "id_j2": self.id_j2,
            "score1": self.score1,
            "score2": self.score2
        }
