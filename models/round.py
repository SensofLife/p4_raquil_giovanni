from typing import Union, List, Dict
from datetime import datetime
import re

from .match import Match


class Round:
    """classe des tours
    """

    def __init__(
        self, name: str, start_date: Union[str, datetime],
        matches: Union[List[Match], List[Dict]], end_date: Union[str, datetime] = None
    ):
        self.name = name
        self.start_date = start_date
        self.end_date = end_date
        self.matches = matches

    def __repr__(self):
        return f"{self.name}"

    @property
    def name(self) -> str:
        return self.__name

    @property
    def start_date(self) -> datetime:
        return self.__start_date

    @property
    def end_date(self) -> datetime:
        return self.__end_date

    @property
    def matches(self) -> List[Match]:
        return self.__matches

    @name.setter
    def name(self, value: str):
        if not isinstance(value, str):
            raise AttributeError("Veuillez rentrez une chaine de caractère ")
        else:
            e = re.match("^([a-zA-Z0-9](\\s?|-?)){2,25}$", value)
            if e:
                self.__name = value.upper()
            else:
                raise AttributeError("Le nom entré n'est pas au bon format")

    @start_date.setter
    def start_date(self, value: Union[str, datetime]):
        if isinstance(value, str):
            try:
                value = datetime.fromisoformat(value)
            except ValueError:
                raise AttributeError("La datetime entré n'est pas au bon format (YYYY-MM-DD)")

        if isinstance(value, datetime):
            # if value <= datetime.today():
            #     raise AttributeError("le tour ne peux pas avoir lieux avant la datetime d'aujourd'hui ")

            self.__start_date = value

    @end_date.setter
    def end_date(self, value: Union[str, datetime]):
        if isinstance(value, str):
            try:
                value = datetime.fromisoformat(value)
            except ValueError:
                raise AttributeError("La datetime entré n'est pas au bon format (YYYY-MM-DD)")

        if isinstance(value, datetime):
            # if value <= self.start_date:
            #     raise AttributeError("le tour ne peux pas ce terminer avant son commencement ")

            self.__end_date = value

    @matches.setter
    def matches(self, value: Union[List[Match], List[Dict]]):
        self.__matches = []
        for x in value:
            if isinstance(x, dict):
                x = Match(**x)
            self.__matches.append(x)

    def serialize(self):
        """
            fonction pour sérialiser la class round
            return: dict
        """
        return {
            "name": self.name,
            "start_date": self.start_date.isoformat(),
            "end_date": self.end_date.isoformat(),
            "matches":  [match.serialize() for match in self.matches]
        }
