from typing import Union
from datetime import date
from enum import Enum
import re
from uuid import UUID, uuid4

from utils import manager


class Player:
    """classe d'un Joueur
    """
    class Genre(Enum):
        Male = "H"
        Female = "F"

        def __str__(self):
            if self is Player.Genre.Male:
                return "H"
            else:
                return "F"

    def __init__(
        self,  last_name: str, first_name: str, birth_date: Union[str, date],
        gender: Union[str, Genre], rank: int, id: Union[str, UUID] = None
    ):
        self.last_name = last_name
        self.first_name = first_name
        self.birth_date = birth_date
        self.gender = gender
        self.rank = rank
        self.id = id

    def __repr__(self):
        return f"({self.last_name} {self.first_name})"

    def __str__(self):
        return f"{self.last_name} {self.first_name}"

    @property
    def last_name(self) -> str:
        return self.__last_name

    @property
    def first_name(self) -> str:
        return self.__first_name

    @property
    def birth_date(self) -> date:
        return self.__birth_date

    @property
    def gender(self) -> Genre:
        return self.__gender

    @property
    def rank(self) -> int:
        return self.__rank

    @property
    def id(self) -> UUID:
        return self.__id

    @last_name.setter
    def last_name(self, value: str):
        if not isinstance(value, str):

            raise AttributeError("Veuillez rentrez une chaine de caractère ")

        else:
            e = re.match("^([a-zA-Z](\\s?|-?)){2,25}$", value)
            if e:
                self.__last_name = value.title()
            else:
                raise AttributeError("Le nom entré n'est pas au bon format")

    @first_name.setter
    def first_name(self, value: str):
        if not isinstance(value, str):
            raise AttributeError("Veuillez rentrez une chaine de caractère ")

        else:
            e = re.match("^([a-zA-Z](\\s?|-?)){2,25}$", value)
            if e is not None:
                self.__first_name = value.title()
            if e is None:
                raise AttributeError("Le prénom entré n'est pas au bon format")

    @birth_date.setter
    def birth_date(self, value: Union[str, date]):
        if isinstance(value, str):
            try:
                value = date.fromisoformat(value)
            except ValueError:
                raise AttributeError("La date entré n'est pas au bon format (YYYY-MM-DD)")

        if isinstance(value, date):
            if (date.today() - value).days//365 < 12:
                raise AttributeError("l'age ne permet pas de participer au tournoi ")

            self.__birth_date = value

        else:
            raise AttributeError("La date entré n'est pas au bon format (YYYY-MM-DD)")

    @gender.setter
    def gender(self, value: Union[str, Genre]):

        if isinstance(value, str):
            try:
                value = Player.Genre(value.upper())
            except ValueError:
                raise AttributeError("""Veuillez rentrer soit:
        -H pour homme
        -F pour Femme""")

        if isinstance(value, Player.Genre):
            self.__gender = value
        else:
            raise AttributeError("Le genre entré n'est pas reconnue")

    @rank.setter
    def rank(self, value: int):
        if isinstance(value, int):
            if 0 <= value <= 3000:
                self.__rank = value
            else:
                raise AttributeError("Veuillez rentré un nombre entre 0 et 3000 ")
        else:
            raise AttributeError("Veuillez rentré un nombre ")

    @id.setter
    def id(self, value: Union[str, UUID]):
        if value is None:
            value = uuid4()

        if isinstance(value, str):
            try:
                value = UUID(value)
            except ValueError:
                raise AttributeError("identifiant invalide ")

        if isinstance(value, UUID):
            if value.version == 4:
                self.__id = value
            else:
                raise AttributeError("identifiant invalide ")

        else:
            raise AttributeError("identifiant invalide ")

    def serialize(self):
        """
            fonction pour sérialiser la class player
            return: dict
        """
        return {
            "id": str(self.id),
            "last_name": self.last_name,
            "first_name": self.first_name,
            "birth_date": self.birth_date.isoformat(),
            "gender": self.gender.value,
            "rank": self.rank
        }


player_manager = manager.Manager(Player, lambda j: str(j.id))
