from datetime import datetime, date

# //////////// models////////////
from models.tournament import tournament_manager
from models.player import player_manager
# /////////// menu /////////////
from views.menu.main_menu import MainMenu
from views.menu.player_menu import PlayerMenu
from views.menu.list_player_menu import ListPlayerMenu
from views.menu.tournament_menu import TournamentMenu
from views.menu.rapport_menu import RapportMenu
from views.menu.score_menu import ScoreMenu
# /////////// menu.choose //////
from views.menu.choose_player_menu import ChoosePlayerMenu
from views.menu.choose_tournament_menu import ChooseTournamentMenu
from views.menu.choose_time import TimeMenu
from views.menu.choose_continu import ContinuMenu
# ////////// form /////////////
from views.form.player_form import PlayerForm
from views.form.tournament_form import TournamentForm
# ////////////////////////////
from utils.router import router
import utils.matchmaking as matchmaking


def main():
    """Menu principale """
    router.navigate(MainMenu().show())


# menu players
def players():
    """Menu joueur """
    router.navigate(PlayerMenu().show())


def create_player():
    """
    permet de creer un objets de la classe joueur en entrant les informations
    """
    player_form = PlayerForm()
    player_data = player_form.show()
    try:
        player_data["birth_date"] = date(
            player_data["birth_date_year"],
            player_data["birth_date_month"],
            player_data["birth_date_day"])

        del player_data["birth_date_year"]
        del player_data["birth_date_month"]
        del player_data["birth_date_day"]
    except ValueError as e:
        print(f"\n!!!{e}!!!\n")
        router.navigate("/players")

    resultat, e = player_manager.create_item(**player_data)
    if not resultat:
        print(f"!!!érreur lors de la création d'un joueur!!!\n{e}\n")
    else:
        player_manager.save()
    router.navigate("/players")


def player_list():
    """ redirection vers la page de selection du type de trie de la liste des joueurs """
    router.navigate(ListPlayerMenu().show())


def players_list_by_name():
    """liste des joueurs pas nom"""
    list_player_name = sorted(player_manager.find_all(), key=lambda x: x.last_name)
    for player_name in list_player_name:
        print(player_name)
    print("\n")
    router.navigate("/players/list")


def players_list_by_rank():
    """liste des joueurs pas classement """
    list_players = sorted(player_manager.find_all(), key=lambda j: j.rank, reverse=True)
    for player in list_players:
        print(f"{player}->classement: {player.rank}")
    print("\n")
    router.navigate("/players/list")


def change_rank():
    """permet de changer le rang d'un joueurs"""
    list_players = player_manager.find_all()
    player_id = ChoosePlayerMenu(list_players).show()
    player = player_manager.find_by_id(str(player_id))
    print(f"{player}->classement: {player.rank}")
    new_rank = input("Veuillez rentrer un nouveaux rang entre 0 et 3000: ")
    print("\n")
    try:
        player.rank = int(new_rank)
        player_manager.update(str(player_id), "rank", int(new_rank))
    except TypeError:
        pass
    router.navigate("/players")


# menu tournament
def tournaments():
    """Menu tournoi """
    router.navigate(TournamentMenu().show())


def create_tournament():
    """permet de creer un objet de la class tournament"""
    tournament_form = TournamentForm()
    tournament_data = tournament_form.show()
    try:
        tournament_data["start_date"] = datetime(
            tournament_data["start_date_year"],
            tournament_data["start_date_month"],
            tournament_data["start_date_day"],
            tournament_data["start_hour"],
            tournament_data["start_minute"])
        del tournament_data["start_date_year"]
        del tournament_data["start_date_month"]
        del tournament_data["start_date_day"]
        del tournament_data["start_hour"]
        del tournament_data["start_minute"]
    except ValueError as e:
        print(f"\n!!!{e}!!!\n")
        router.navigate("/tournaments")

    try:
        tournament_data["end_date"] = datetime(
            tournament_data["end_date_year"],
            tournament_data["end_date_month"],
            tournament_data["end_date_day"],
            tournament_data["end_hour"],
            tournament_data["end_minute"])

        del tournament_data["end_date_year"]
        del tournament_data["end_date_month"]
        del tournament_data["end_date_day"]
        del tournament_data["end_hour"]
        del tournament_data["end_minute"]
    except ValueError as e:
        print(f"\n!!!{e}!!!\n")
        router.navigate("/tournaments")

    list_players = player_manager.find_all()
    tournament_data["players"] = []
    for i in range(tournament_data["nb_of_player"]):
        player_id = ChoosePlayerMenu(list_players).show()
        tournament_data["players"].append(str(player_id))
        list_players.remove(player_manager.find_by_id(str(player_id)))
    del tournament_data["nb_of_player"]

    tournament_data["rounds"] = []

    tournament_data["time"] = TimeMenu().show()

    resultat, e = tournament_manager.create_item(**tournament_data)
    if not resultat:
        print(f"!!!érreur lors de la création d'un tournoi!!!\n{e}\n")
    else:
        tournament_manager.save()
    router.navigate("/tournaments")


def tournaments_list():
    """permet d'avoir la liste des tournois"""
    tournaments = sorted(tournament_manager.find_all(), key=lambda x: x.name)
    if tournaments == []:
        print("Il n'y a pas de tournoi enregistré\n")
    else:
        for tournament in tournaments:
            print(tournament.id)
    print("\n")
    router.navigate("/tournaments")


def tournaments_play():
    """permet de lancer un tournoi"""
    list_tournament = [t for t in tournament_manager.find_all() if not t.is_over]
    tournament_id = ChooseTournamentMenu(list_tournament).show()
    if tournament_id is None:
        router.navigate("/tournaments")
    tournament_choose = tournament_manager.find_by_id(tournament_id)
    print("lancement du tournament \n")

    i = len(tournament_choose.rounds)
    if i == 0:
        round_ = matchmaking.generate_round(tournament_choose)
        score_entrance(round_, tournament_choose)
        print("fin du tour1 \n")
        tournament_manager.save()
        continue_()

    for x in range(1, tournament_choose.nb_of_round):
        round_ = matchmaking.generate_round(tournament_choose)
        score_entrance(round_, tournament_choose)
        tournament_manager.save()
        if x < (tournament_choose.nb_of_round-1):
            continue_()

    router.navigate("/tournaments")


def continue_():
    """choix de continuer ou non un tournoi lançé"""
    choice = ContinuMenu().show()
    if choice == "O":
        print("lancement du tour suivant.\n")
    elif choice == "N":
        router.navigate("/tournaments")


def score_entrance(rnd, tournament):
    """permet de rentrer les score des match d'un round
        param A(class Round)
        param B(class tournament)
    """

    for i in rnd.matches:
        finish = 0
        while finish == 0:
            nom_j1 = player_manager.find_by_id(i.id_j1)
            nom_j2 = player_manager.find_by_id(i.id_j2)

            print(f"entrez le score du match: {nom_j1} VS {nom_j2} :")
            score_j1 = ScoreMenu(nom_j1, nom_j2).show()
            score_j2 = 1.0 - score_j1
            print(f"{score_j1}, {score_j2}")

            i.score1 += score_j1
            i.score2 += score_j2
            finish = 1
    rnd.end_date = datetime.now()
    tournament.rounds.append(rnd)


# menu rapport
def rapport():
    """Menu rapport """
    router.navigate(RapportMenu().show())


def rapport_player_list():
    """permet d 'avoir la liste de tout les joueurs"""
    router.navigate("/players/list")


def rapport_tournament_list():
    """permet d 'avoir la liste de tout les tournois"""
    tournaments = sorted(tournament_manager.find_all(), key=lambda x: x.name)
    if tournaments == []:
        print("Il n'y a pas de tournoi enregistré\n")
    else:
        for tournament in tournaments:
            print(tournament)
        print("\n")
    router.navigate("/rapport")


def find_tournament():
    """permet de retourner l'ID d'un tournoi choisie par l'utilisateur
        return A(str):
            id du tournoi choisi"""
    list_tournament = [t for t in tournament_manager.find_all()]
    tournament_id = ChooseTournamentMenu(list_tournament).show()
    return tournament_id


def rapport_tournament_player_list():
    """permet d'avoir une liste des joueurs d'un tournoi choisie par l'utilisateur"""
    tournament_choose = find_tournament()
    if tournament_choose is not None:
        t = tournament_manager.find_by_id(tournament_choose)
        list_matches = [matches for rnd in t.rounds for matches in rnd.matches]
        player_sorted = matchmaking.sorted_player_by_score(t, list_matches)
        for player in player_sorted:
            score_player = matchmaking.get_player_score(player.id, list_matches)
            print(player, score_player)
        print("\n")
    else:
        pass
    router.navigate("/rapport")


def rapport_tournament_list_round():
    """permet d'avoir la liste des tours ainsi que les matchs associés"""
    tournament_choose = find_tournament()
    if tournament_choose is not None:
        t = tournament_manager.find_by_id(tournament_choose)
        for rnd in t.rounds:
            print(f"----{rnd}----")
            for match in rnd.matches:
                nom_j1 = player_manager.find_by_id(match.id_j1)
                nom_j2 = player_manager.find_by_id(match.id_j2)
                print(f"{nom_j1} VS {nom_j2} -> {match.score1}--{match.score2}")
        print("\n")
    else:
        print("aucun tour n'a été joué")
        pass
    router.navigate("/rapport")
